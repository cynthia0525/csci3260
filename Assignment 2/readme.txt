	CSCI3260 Assignment 2 Texture Mapping / Lighting Control  

Name: CHAN Ka Yi
Student ID: 1155093641

Manipulation:
	// Lighting control
	Key "q"     :	increase the brightness of diffuse light
	Key "w"     :	decrease the brightness of diffuse light
	Key "z"     :	increase the brightness of specular light
	Key "x"     :	decrease the brightness of specular light

	// Texture control
	Key "1"     :	change the texture of floor to default
	Key "2"     :	change the texture of floor to wood
	Key "3"     :	change the texture of floor to carpet

	// Model control
	Key "s"     :	toggle the animation of piano
	Key "up"    :	translate the trumpet forewards
	Key "down"  :	translate the trumpet backwards
	Key "left"  :	rotate the trumpet clockwise along y-axis
	Key "right" :	rotate the trumpet anti-clockwise along y-axis

	// Camera control
	Key " "     :	toggle the control of camera
	Sroll up    :	move the scene downwards
	Scroll left :	move the scene rightwards