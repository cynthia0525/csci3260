#version 430 // GLSL version your computer supports

#define directional 1
#define spot 2
#define point 1

in vec3 vertexFrag;
in vec2 uvFrag;
in vec3 normalFrag;

uniform sampler2D mapping;

// keyboard control
uniform float abientControl;
uniform float diffuseControl;
uniform float specularControl;

// light parameter
uniform vec3 viewPort;
uniform vec3 lightSource[directional + spot + point];
uniform vec3 lightColor[directional + spot + point];
uniform vec3 lightDirection[spot];
uniform float lightCutoff[spot];

// attenuation coeffient
float constantCoeffient = 0.3f;
float linearCoeffient = 0.001f;
float quadraticCoeffient = 0.001f;

out vec4 colorFrag;

void main()
{
	vec3 ambient = vec3(0.0f);
	vec3 diffuse = vec3(0.0f);
	vec3 specular = vec3(0.0f);
	float brightness = 0;
	float shininess = 0;
	int num = directional + spot + point;

	// material color
	vec3 ambientColor = texture(mapping, uvFrag).rgb;
	vec3 diffuseColor = texture(mapping, uvFrag).rgb;
	vec3 specularColor = vec3(1.0f, 1.0f, 1.0f);

	for (int i = 0; i < num; i++) {
		// attenuation
		float distance = length(lightSource[i] - vertexFrag);
		float attenuation = constantCoeffient + linearCoeffient * distance + quadraticCoeffient * distance * distance;

		// diffuse
		vec3 normalVector = normalize(normalFrag);
		vec3 lightVector = normalize(lightSource[i] - vertexFrag);
		/* directional light */
		if (i == 0) {
			lightVector = normalize(-lightSource[i]);
			attenuation = 1;
		}
		brightness += clamp(dot(normalVector, lightVector), 0, 1) / attenuation;

		// specular
		vec3 reflectVector = reflect(-lightVector, normalVector);
		vec3 eyeVector = normalize(viewPort - vertexFrag);
		shininess += clamp(dot(reflectVector, eyeVector), 0, 1);

		// spot light
		float intensity = 1;
		if (i >= directional && i < directional + spot) {
			float theta = dot(-lightVector, normalize(-lightDirection[i - 1]));
			if (theta > lightCutoff[i - 1])
				intensity = 1 - (1 - theta) / (1 - lightCutoff[i - 1]);
			else intensity = 0;
		}

		// object color
		ambient += ambientColor * lightColor[i] * intensity;
		diffuse += diffuseColor * lightColor[i] * brightness * (intensity / attenuation);
		specular += specularColor * lightColor[i] * pow(shininess, 10) * (intensity / attenuation);
	}

	vec3 color = ambient * abientControl + diffuse * diffuseControl + specular * specularControl;
	colorFrag = vec4(color, 1.0);
}
