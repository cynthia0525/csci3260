/*********************************************************
FILE : main.cpp (csci3260 2018-2019 Assignment 2)
*********************************************************/
/*********************************************************
Student Information
Student ID: 1155093641
Student Name: CHAN Ka Yi
*********************************************************/

#define _CRT_SECURE_NO_WARNINGS

#include "Dependencies\glew\glew.h"
#include "Dependencies\freeglut\freeglut.h"
#include "Dependencies\glm\glm.hpp"
#include "Dependencies\glm\gtc\matrix_transform.hpp"
#include "Dependencies\glm\gtc\type_ptr.hpp"
#include <iostream>
#include <fstream>
#include <vector>
using namespace std;
using glm::vec3;
using glm::vec4;
using glm::mat4;

GLint programID;
char path[20];
// object [vao / vbo / texture]
GLuint floor_vao, floor_vbo, floor_uvbo, floor_nbo, floor_texture;
int floorSize;
GLuint drum_vao, drum_vbo, drum_uvbo, drum_nbo, drum_texture;
int drumSize;
GLuint piano_vao, piano_vbo, piano_uvbo, piano_nbo, piano_texture;
int pianoSize;
GLuint trumpet_vao, trumpet_vbo, trumpet_uvbo, trumpet_nbo, trumpet_texture;
int trumpetSize;
// model transformation [trumpet]
int z_translate = 0;
float y_rotate = 0;
int y_correct = 0;
// model control [piano]
int model_control = -1;
float model_rotate = 0;
int model_correct = 0;
// view control
int view_control = -1;
float x_pos = 0;
float y_pos = 0;
// light control
float diffuse = 0.5;
float specular = 0.5;

GLuint loadBMP_custom(const char *);

//a series utilities for setting shader parameters 
void setMat4(const std::string &name, glm::mat4& value)
{
	unsigned int transformLoc = glGetUniformLocation(programID, name.c_str());
	glUniformMatrix4fv(transformLoc, 1, GL_FALSE, glm::value_ptr(value));
}

void setVec4(const std::string &name, glm::vec4 value)
{
	glUniform4fv(glGetUniformLocation(programID, name.c_str()), 1, &value[0]);
}
void setVec3(const std::string &name, glm::vec3 value)
{
	glUniform3fv(glGetUniformLocation(programID, name.c_str()), 1, &value[0]);
}
void setFloat(const std::string &name, float value)
{
	glUniform1f(glGetUniformLocation(programID, name.c_str()), value);
}
void setInt(const std::string &name, int value)
{
	glUniform1i(glGetUniformLocation(programID, name.c_str()), value);
}

bool checkStatus(
	GLuint objectID,
	PFNGLGETSHADERIVPROC objectPropertyGetterFunc,
	PFNGLGETSHADERINFOLOGPROC getInfoLogFunc,
	GLenum statusType)
{
	GLint status;
	objectPropertyGetterFunc(objectID, statusType, &status);
	if (status != GL_TRUE)
	{
		GLint infoLogLength;
		objectPropertyGetterFunc(objectID, GL_INFO_LOG_LENGTH, &infoLogLength);
		GLchar* buffer = new GLchar[infoLogLength];

		GLsizei bufferSize;
		getInfoLogFunc(objectID, infoLogLength, &bufferSize, buffer);
		cout << buffer << endl;

		delete[] buffer;
		return false;
	}
	return true;
}

bool checkShaderStatus(GLuint shaderID)
{
	return checkStatus(shaderID, glGetShaderiv, glGetShaderInfoLog, GL_COMPILE_STATUS);
}

bool checkProgramStatus(GLuint programID)
{
	return checkStatus(programID, glGetProgramiv, glGetProgramInfoLog, GL_LINK_STATUS);
}

string readShaderCode(const char* fileName)
{
	ifstream meInput(fileName);
	if (!meInput.good())
	{
		cout << "File failed to load..." << fileName;
		exit(1);
	}
	return std::string(
		std::istreambuf_iterator<char>(meInput),
		std::istreambuf_iterator<char>()
	);
}

void installShaders()
{
	GLuint vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar* adapter[1];
	string temp = readShaderCode("VertexShaderCode.glsl");
	adapter[0] = temp.c_str();
	glShaderSource(vertexShaderID, 1, adapter, 0);
	temp = readShaderCode("FragmentShaderCode.glsl");
	adapter[0] = temp.c_str();
	glShaderSource(fragmentShaderID, 1, adapter, 0);

	glCompileShader(vertexShaderID);
	glCompileShader(fragmentShaderID);

	if (!checkShaderStatus(vertexShaderID) || !checkShaderStatus(fragmentShaderID))
		return;

	programID = glCreateProgram();
	glAttachShader(programID, vertexShaderID);
	glAttachShader(programID, fragmentShaderID);
	glLinkProgram(programID);

	if (!checkProgramStatus(programID))
		return;

	glDeleteShader(vertexShaderID);
	glDeleteShader(fragmentShaderID);

	glUseProgram(programID);
}

void keyboard(unsigned char key, int x, int y)
{
	// texture control
	if (key == '1') {
		strcpy(path, "texture/default.bmp");
		floor_texture = loadBMP_custom(path);
	}
	if (key == '2') {
		strcpy(path, "texture/wood.bmp");
		floor_texture = loadBMP_custom(path);
	}
	if (key == '3') {
		strcpy(path, "texture/carpet.bmp");
		floor_texture = loadBMP_custom(path);
	}

	// light control
	if (key == 'q') {
		diffuse += 0.1;
		if (diffuse > 1)
			diffuse -= 0.1;
	}
	if (key == 'w') {
		diffuse -= 0.1;
		if (diffuse < 0)
			diffuse += 0.1;
	}
	if (key == 'z') {
		specular += 0.1;
		if (specular > 1)
			specular -= 0.1;
	}
	if (key == 'x') {
		specular -= 0.1;
		if (specular < 0)
			specular += 0.1;
	}

	// animation control
	if (key == 's') {
		model_control *= -1;
	}
	if (key == ' ') {
		view_control *= -1;
	}
}

void move(int key, int x, int y)
{
	// model translation
	if (key == GLUT_KEY_UP) {
		z_translate += 1;
	}
	if (key == GLUT_KEY_DOWN) {
		z_translate -= 1;
	}

	// model rotation
	if (key == GLUT_KEY_LEFT) {
		y_rotate -= 1;
		if (y_rotate < -90) {
			y_correct -= 1;
			y_rotate += 90;
		}
	}
	if (key == GLUT_KEY_RIGHT) {
		y_rotate += 1;
		if (y_rotate > +90) {
			y_correct += 1;
			y_rotate -= 90;
		}
	}

	// rotation animation
	if (key == GLUT_KEY_HOME) {
		model_rotate += 0.5f;
		if (model_rotate > +90) {
			model_correct += 1;
			model_rotate -= 90;
		}
	}
}

void PassiveMouse(int x, int y)
{
	if (view_control == 1) {
		x_pos += (float)(720 - x) / 360;
		y_pos += (float)(540 - y) / 270;
	}
}

bool loadOBJ(
	const char * path,
	std::vector<glm::vec3> & out_vertices,
	std::vector<glm::vec2> & out_uvs,
	std::vector<glm::vec3> & out_normals
) {
	printf("Loading OBJ file %s...\n", path);

	std::vector<unsigned int> vertexIndices, uvIndices, normalIndices;
	std::vector<glm::vec3> temp_vertices;
	std::vector<glm::vec2> temp_uvs;
	std::vector<glm::vec3> temp_normals;


	FILE * file = fopen(path, "r");
	if (file == NULL) {
		printf("Impossible to open the file ! Are you in the right path ? See Tutorial 6 for details\n");
		getchar();
		return false;
	}

	while (1) {

		char lineHeader[128];
		int res = fscanf(file, "%s", lineHeader);
		if (res == EOF)
			break; // EOF = End Of File. Quit the loop.

				   // else : parse lineHeader

		if (strcmp(lineHeader, "v") == 0) {
			glm::vec3 vertex;
			fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
			temp_vertices.push_back(vertex);
		}
		else if (strcmp(lineHeader, "vt") == 0) {
			glm::vec2 uv;
			fscanf(file, "%f %f\n", &uv.x, &uv.y);
			uv.y = -uv.y;
			temp_uvs.push_back(uv);
		}
		else if (strcmp(lineHeader, "vn") == 0) {
			glm::vec3 normal;
			fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
			temp_normals.push_back(normal);
		}
		else if (strcmp(lineHeader, "f") == 0) {
			std::string vertex1, vertex2, vertex3, vertex4;
			unsigned int vertexIndex[4], uvIndex[4], normalIndex[4];
			int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2], &vertexIndex[3], &uvIndex[3], &normalIndex[3]);
			if (!(matches == 9 || matches == 12)) {
				printf("File can't be read by our simple parser :-( Try exporting with other options\n");
				return false;
			}
			vertexIndices.push_back(vertexIndex[0]);
			vertexIndices.push_back(vertexIndex[1]);
			vertexIndices.push_back(vertexIndex[2]);
			uvIndices.push_back(uvIndex[0]);
			uvIndices.push_back(uvIndex[1]);
			uvIndices.push_back(uvIndex[2]);
			normalIndices.push_back(normalIndex[0]);
			normalIndices.push_back(normalIndex[1]);
			normalIndices.push_back(normalIndex[2]);
			if (matches == 12) {
				vertexIndices.push_back(vertexIndex[1]);
				vertexIndices.push_back(vertexIndex[2]);
				vertexIndices.push_back(vertexIndex[3]);
				uvIndices.push_back(uvIndex[1]);
				uvIndices.push_back(uvIndex[2]);
				uvIndices.push_back(uvIndex[3]);
				normalIndices.push_back(normalIndex[1]);
				normalIndices.push_back(normalIndex[2]);
				normalIndices.push_back(normalIndex[3]);
			}
		}
		else {
			char stupidBuffer[1000];
			fgets(stupidBuffer, 1000, file);
		}

	}

	// For each vertex of each triangle
	for (unsigned int i = 0; i<vertexIndices.size(); i++) {

		// Get the indices of its attributes
		unsigned int vertexIndex = vertexIndices[i];
		unsigned int uvIndex = uvIndices[i];
		unsigned int normalIndex = normalIndices[i];

		// Get the attributes thanks to the index
		glm::vec3 vertex = temp_vertices[vertexIndex - 1];
		glm::vec2 uv = temp_uvs[uvIndex - 1];
		glm::vec3 normal = temp_normals[normalIndex - 1];

		// Put the attributes in buffers
		out_vertices.push_back(vertex);
		out_uvs.push_back(uv);
		out_normals.push_back(normal);

	}

	return true;
}

GLuint loadBMP_custom(const char * imagepath) {

	printf("Reading image %s\n", imagepath);

	unsigned char header[54];
	unsigned int dataPos;
	unsigned int imageSize;
	unsigned int width, height;
	unsigned char * data;

	FILE * file = fopen(imagepath, "rb");
	if (!file) { printf("%s could not be opened. Are you in the right directory ? Don't forget to read the FAQ !\n", imagepath); getchar(); return 0; }

	if (fread(header, 1, 54, file) != 54) {
		printf("Not a correct BMP file\n");
		return 0;
	}
	if (header[0] != 'B' || header[1] != 'M') {
		printf("Not a correct BMP file\n");
		return 0;
	}
	if (*(int*)&(header[0x1E]) != 0) { printf("Not a correct BMP file\n");    return 0; }
	if (*(int*)&(header[0x1C]) != 24) { printf("Not a correct BMP file\n");    return 0; }

	dataPos = *(int*)&(header[0x0A]);
	imageSize = *(int*)&(header[0x22]);
	width = *(int*)&(header[0x12]);
	height = *(int*)&(header[0x16]);
	if (imageSize == 0)    imageSize = width*height * 3;
	if (dataPos == 0)      dataPos = 54;

	data = new unsigned char[imageSize];
	fread(data, 1, imageSize, file);
	fclose(file);

	GLuint textureID;
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);
	delete[] data;

	return textureID;
}

void sendDataToOpenGL()
{
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;
	bool res;

	// load floor obj
	res = loadOBJ("object/plane.obj", vertices, uvs, normals);
	floorSize = vertices.size();
	// load default texture
	strcpy(path, "texture/default.bmp");
	floor_texture = loadBMP_custom(path);
	// create floor vao
	glGenVertexArrays(1, &floor_vao);
	glBindVertexArray(floor_vao);
	// create floor position vbo
	glGenBuffers(1, &floor_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, floor_vbo);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	// create floor texture vbo
	glGenBuffers(1, &floor_uvbo);
	glBindBuffer(GL_ARRAY_BUFFER, floor_uvbo);
	glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
	// create floor normal vbo
	glGenBuffers(1, &floor_nbo);
	glBindBuffer(GL_ARRAY_BUFFER, floor_nbo);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	vertices.clear();
	uvs.clear();
	normals.clear();

	// load drum obj
	res = loadOBJ("object/drum.obj", vertices, uvs, normals);
	drum_texture = loadBMP_custom("texture/rubber.bmp");
	drumSize = vertices.size();
	// create drum vao
	glGenVertexArrays(1, &drum_vao);
	glBindVertexArray(drum_vao);
	// create drum position vbo
	glGenBuffers(1, &drum_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, drum_vbo);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	// create drum texture vbo
	glGenBuffers(1, &drum_uvbo);
	glBindBuffer(GL_ARRAY_BUFFER, drum_uvbo);
	glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
	// create drum normal vbo
	glGenBuffers(1, &drum_nbo);
	glBindBuffer(GL_ARRAY_BUFFER, drum_nbo);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	vertices.clear();
	uvs.clear();
	normals.clear();

	// load piano obj
	res = loadOBJ("object/piano.obj", vertices, uvs, normals);
	piano_texture = loadBMP_custom("texture/koa.bmp");
	pianoSize = vertices.size();
	// create piano vao
	glGenVertexArrays(1, &piano_vao);
	glBindVertexArray(piano_vao);
	// create piano position vbo
	glGenBuffers(1, &piano_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, piano_vbo);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	// create piano texture vbo
	glGenBuffers(1, &piano_uvbo);
	glBindBuffer(GL_ARRAY_BUFFER, piano_uvbo);
	glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
	// create piano normal vbo
	glGenBuffers(1, &piano_nbo);
	glBindBuffer(GL_ARRAY_BUFFER, piano_nbo);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	vertices.clear();
	uvs.clear();
	normals.clear();

	// load trumpet obj
	res = loadOBJ("object/trumpet.obj", vertices, uvs, normals);
	trumpet_texture = loadBMP_custom("texture/brass.bmp");
	trumpetSize = vertices.size();
	// create trumpet vao
	glGenVertexArrays(1, &trumpet_vao);
	glBindVertexArray(trumpet_vao);
	// create trumpet position vbo
	glGenBuffers(1, &trumpet_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, trumpet_vbo);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	// create trumpet texture vbo
	glGenBuffers(1, &trumpet_uvbo);
	glBindBuffer(GL_ARRAY_BUFFER, trumpet_uvbo);
	glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
	// create trumpet normal vbo
	glGenBuffers(1, &trumpet_nbo);
	glBindBuffer(GL_ARRAY_BUFFER, trumpet_nbo);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	vertices.clear();
	uvs.clear();
	normals.clear();
}

void paintGL(void)
{
	glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
	glEnable(GL_DEPTH_TEST | GL_CULL_FACE);
	glDepthFunc(GL_LESS);
	glCullFace(GL_FRONT);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if (model_control == 1) {
		move(GLUT_KEY_HOME, 0, 0);
	}

	// Projection matrix
	glm::mat4 projectionMatrix = glm::perspective(
		glm::radians(45.0f),
		4.0f / 3.0f,
		0.1f,
		800.0f
	);
	// View matrix
	glm::mat4 viewMatrix = glm::lookAt(
		glm::vec3(300 * sin(glm::radians(x_pos)), 50 * cos(glm::radians(y_pos)), 300 * cos(glm::radians(x_pos)) - 300 * sin(glm::radians(y_pos))),
		glm::vec3(0, 0, 0),
		glm::vec3(0, 1, 0)
	);
	// Model matrix
	glm::mat4 translateMatrix = glm::translate(mat4(), vec3(0, -30, 0));
	glm::mat4 rotateMatrix = glm::rotate(mat4(), 0.0f, vec3(0, 1, 0));
	glm::mat4 scaleMatrix = glm::scale(mat4(), vec3(18.0f, 18.0f, 18.0f));

	// projection
	glm::mat4 projection = projectionMatrix * viewMatrix;
	GLuint projectionUniformLocation = glGetUniformLocation(programID, "projection");
	glUniformMatrix4fv(projectionUniformLocation, 1, GL_FALSE, &projection[0][0]);

	// model
	glm::mat4 model = translateMatrix * scaleMatrix;
	GLuint modelUniformLocation = glGetUniformLocation(programID, "model");
	glUniformMatrix4fv(modelUniformLocation, 1, GL_FALSE, &model[0][0]);

	// mapping
	GLuint textureID = glGetUniformLocation(programID, "mapping");
	glUniform1i(textureID, 0);

	// lighting
	vec3 lightSource, lightColor, lightDirection;
	glUniform1f(glGetUniformLocation(programID, "abientControl"), 0.9f);
	glUniform1f(glGetUniformLocation(programID, "diffuseControl"), diffuse);
	glUniform1f(glGetUniformLocation(programID, "specularControl"), specular * 0.0005f);
	GLint viewPortUniformLocation = glGetUniformLocation(programID, "viewPort");
	vec3 viewPort(0.0f, 50.0f, 300.0f);
	glUniform3fv(viewPortUniformLocation, 1, &viewPort[0]);
	// blue directional light
	lightSource = vec3(0.0f, 100.0f, -180.0f);
	glUniform4fv(glGetUniformLocation(programID, "lightSource[0]"), 1, &lightSource[0]);
	lightColor = vec3(0.18f, 0.18f, 0.3f);
	glUniform3fv(glGetUniformLocation(programID, "lightColor[0]"), 1, &lightColor[0]);
	// bronze spot light
	lightSource = vec3(-130.0f, 100.0f, 100.0f);
	glUniform4fv(glGetUniformLocation(programID, "lightSource[1]"), 1, &lightSource[0]);
	lightColor = vec3(0.93f, 0.93f, 0.68f);
	glUniform3fv(glGetUniformLocation(programID, "lightColor[1]"), 1, &lightColor[0]);
	lightDirection = lightSource - vec3(150.0f, 0.0f, 180.0f);
	glUniform3fv(glGetUniformLocation(programID, "lightDirection[0]"), 1, &lightDirection[0]);
	glUniform1f(glGetUniformLocation(programID, "lightCutoff[0]"), glm::cos(glm::radians(30.0f)));
	// bronze spot light
	lightSource = vec3(130.0f, 100.0f, 100.0f);
	glUniform4fv(glGetUniformLocation(programID, "lightSource[2]"), 1, &lightSource[0]);
	lightColor = vec3(0.93f, 0.93f, 0.68f);
	glUniform3fv(glGetUniformLocation(programID, "lightColor[2]"), 1, &lightColor[0]);
	lightDirection = lightSource - vec3(-150.0f, 0.0f, 20.0f);
	glUniform3fv(glGetUniformLocation(programID, "lightDirection[1]"), 1, &lightDirection[0]);
	glUniform1f(glGetUniformLocation(programID, "lightCutoff[1]"), glm::cos(glm::radians(30.0f)));
	// blue point light
	lightSource = vec3(0.0f, 250.0f, 0.0f);
	glUniform4fv(glGetUniformLocation(programID, "lightSource[3]"), 1, &lightSource[0]);
	lightColor = vec3(0.18f, 0.18f, 0.3f);
	glUniform3fv(glGetUniformLocation(programID, "lightColor[3]"), 1, &lightColor[0]);

	// Floor object
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, floor_texture);
	glBindVertexArray(floor_vao);
	glDrawArrays(GL_TRIANGLES, 0, floorSize);
	
	translateMatrix = glm::translate(mat4(), vec3(-300, 0, -30));
	scaleMatrix = glm::scale(mat4(), vec3(0.2f, 0.2f, 0.2f));
	model = translateMatrix * scaleMatrix;
	glUniformMatrix4fv(modelUniformLocation, 1, GL_FALSE, &model[0][0]);
	glUniform1i(textureID, 0);
	// Drum object
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, drum_texture);
	glBindVertexArray(drum_vao);
	glDrawArrays(GL_TRIANGLES, 0, drumSize);

	translateMatrix = glm::translate(mat4(), vec3(80, -25, 30));
	rotateMatrix = glm::rotate(mat4(), glm::radians(-90.0f), vec3(1, 0, 0)) * glm::rotate(mat4(), model_correct + sin(glm::radians(model_rotate)), vec3(0, 0, 1));
	scaleMatrix = glm::scale(mat4(), vec3(0.25f, 0.25f, 0.25f));
	model = translateMatrix * rotateMatrix * scaleMatrix;
	glUniformMatrix4fv(modelUniformLocation, 1, GL_FALSE, &model[0][0]);
	glUniform1i(textureID, 0);
	// Piano object
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, piano_texture);
	glBindVertexArray(piano_vao);
	glDrawArrays(GL_TRIANGLES, 0, pianoSize);

	translateMatrix = glm::translate(mat4(), vec3(0, -10, 50 + z_translate));
	rotateMatrix = glm::rotate(mat4(), y_correct + sin(glm::radians(y_rotate)), vec3(0, 1, 0));
	model = translateMatrix * rotateMatrix;
	glUniformMatrix4fv(modelUniformLocation, 1, GL_FALSE, &model[0][0]);
	glUniform1i(textureID, 0);
	// Trumpet object
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, trumpet_texture);
	glBindVertexArray(trumpet_vao);
	glDrawArrays(GL_TRIANGLES, 0, trumpetSize);

	glFlush();
	glutPostRedisplay();
}

void initializedGL(void) //run only once
{
	glewInit();
	installShaders();
	sendDataToOpenGL();
}

int main(int argc, char *argv[])
{
	glutInit(&argc, argv);
	glutInitWindowPosition(50, 50);
	glutInitWindowSize(720, 540);
	glutCreateWindow("Assignment 2");

	initializedGL();
	glutDisplayFunc(paintGL);

	glutKeyboardFunc(keyboard);
	glutSpecialFunc(move);
	glutPassiveMotionFunc(PassiveMouse);

	glutMainLoop();

	return 0;
}