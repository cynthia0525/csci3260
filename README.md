# CSCI3260 Principles of Computer Graphics (Fall 2018)
Fundamental computer graphics techniques and algorithms will be introduced. Topics to be covered include: graphics hardware and interaction devices, transformation of coordination systems, scan conversion algorithms, hidden surface algorithms, illumination models and shading, rendering, texture mapping, computer animation and visualization.

# Grading
* 10% Assignment 1 [90/100]
* 15% Assignment 2 [90/100]
* 20% Course Project [100/100]
* 25% Mid-term Exam
* 40% Final Exam

# Remarks
To successfully run the program (.exe), please download all the materials including objects (.obj), textures (.bmp), shaders (.glsl) and libraries (.dll).