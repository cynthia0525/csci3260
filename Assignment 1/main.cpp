///////////////////////////////////////////
////Type your name and student ID here/////
////Name: CHAN Ka Yi
////Student ID: 1155093641

#include "Dependencies\glew\glew.h"
#include "Dependencies\freeglut\freeglut.h"
#include "Dependencies\glm\glm.hpp"
#include "Dependencies\glm\gtc\matrix_transform.hpp"
#include <iostream>
#include <fstream>
using namespace std;
using glm::vec3;
using glm::mat4;

GLint programID;
const float delta = 0.1f;
int x_translate = 0;		// translation
int y_translate = 0;
float horizontal = 0;		// rotation
float vertical = 0;
float x_rotate, y_rotate;
int x_correct = 0;			// correct 90 degree error
int y_correct = 0;
int scale = 0;				// scaling

GLuint cake_vao, cake_vbo[2], cake_indices_vbo;				// cake object (3D)
GLuint berry_vao, berry_vbo[2];								// berry object (Points)
GLuint topping_vao, topping_vbo[2], topping_indices_vbo;	// topping obect (2D)
GLuint tag_vao, tag_vbo[2], tag_indices_vbo;				// tag object (Lines)

bool checkStatus(
	GLuint objectID,
	PFNGLGETSHADERIVPROC objectPropertyGetterFunc,
	PFNGLGETSHADERINFOLOGPROC getInfoLogFunc,
	GLenum statusType)
{
	GLint status;
	objectPropertyGetterFunc(objectID, statusType, &status);
	if (status != GL_TRUE)
	{
		GLint infoLogLength;
		objectPropertyGetterFunc(objectID, GL_INFO_LOG_LENGTH, &infoLogLength);
		GLchar* buffer = new GLchar[infoLogLength];

		GLsizei bufferSize;
		getInfoLogFunc(objectID, infoLogLength, &bufferSize, buffer);
		cout << buffer << endl;

		delete[] buffer;
		return false;
	}
	return true;
}

bool checkShaderStatus(GLuint shaderID)
{
	return checkStatus(shaderID, glGetShaderiv, glGetShaderInfoLog, GL_COMPILE_STATUS);
}

bool checkProgramStatus(GLuint programID)
{
	return checkStatus(programID, glGetProgramiv, glGetProgramInfoLog, GL_LINK_STATUS);
}

string readShaderCode(const char* fileName)
{
	ifstream meInput(fileName);
	if (!meInput.good())
	{
		cout << "File failed to load..." << fileName;
		exit(1);
	}
	return std::string(
		std::istreambuf_iterator<char>(meInput),
		std::istreambuf_iterator<char>()
	);
}

void installShaders()
{
	GLuint vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar* adapter[1];
	//adapter[0] = vertexShaderCode;
	string temp = readShaderCode("VertexShaderCode.glsl");
	adapter[0] = temp.c_str();
	glShaderSource(vertexShaderID, 1, adapter, 0);
	//adapter[0] = fragmentShaderCode;
	temp = readShaderCode("FragmentShaderCode.glsl");
	adapter[0] = temp.c_str();
	glShaderSource(fragmentShaderID, 1, adapter, 0);

	glCompileShader(vertexShaderID);
	glCompileShader(fragmentShaderID);

	if (!checkShaderStatus(vertexShaderID) || !checkShaderStatus(fragmentShaderID))
		return;

	programID = glCreateProgram();
	glAttachShader(programID, vertexShaderID);
	glAttachShader(programID, fragmentShaderID);
	glLinkProgram(programID);

	if (!checkProgramStatus(programID))
		return;

	glDeleteShader(vertexShaderID);
	glDeleteShader(fragmentShaderID);

	glUseProgram(programID);
}

void mouse(int button, int state, int x, int y)
{
	// scaling
	if (button == 3) {
		scale += 1;
	}
	if (button == 4) {
		scale -= 1;
		if (scale <= -8) scale += 1;	// avoid upside down
	}
}

void specialKey(int key, int x, int y)
{
	// rotation
	if (key == GLUT_KEY_LEFT) {
		vertical -= 1;
		if (vertical < -90) {
			y_correct -= 1;
			vertical += 90;
		}
	}
	if (key == GLUT_KEY_UP) {
		horizontal -= 1;
		if (horizontal < -90) {
			x_correct -= 1;
			horizontal += 90;
		}
	}
	if (key == GLUT_KEY_DOWN) {
		horizontal += 1;
		if (horizontal > +90) {
			x_correct += 1;
			horizontal -= 90;
		}
	}
	if (key == GLUT_KEY_RIGHT) {
		vertical += 1;
		if (vertical > +90) {
			y_correct += 1;
			vertical -= 90;
		}
	}
}

void keyboard(unsigned char key, int x, int y)
{
	// translation
	if (key == 'a') {
		x_translate -= 1;
	}
	if (key == 'w') {
		y_translate += 1;
	}
	if (key == 's') {
		y_translate -= 1;
	}
	if (key == 'd') {
		x_translate += 1;
	}
}

void sendDataToOpenGL()
{
	const GLfloat cake_vertices[] = {
		+1.0f, -0.5f, +2.5f,	// Near rectangle
		+1.0f, +0.5f, +2.5f,
		-1.0f, +0.5f, +2.5f,
		-1.0f, -0.5f, +2.5f,
		+1.0f, -0.5f, -2.5f,	// Far rectangle
		+1.0f, +0.5f, -2.5f,
		-1.0f, +0.5f, -2.5f,
		-1.0f, -0.5f, -2.5f,
	};
	const GLfloat cake_colors[] = {
		1.00f, 1.00f, 1.00f,	// Near rectangle
		0.36f, 0.25f, 0.20f,
		0.36f, 0.25f, 0.20f,
		1.00f, 1.00f, 1.00f,
		1.00f, 1.00f, 1.00f,	// Far rectangle
		0.36f, 0.25f, 0.20f,
		0.36f, 0.25f, 0.20f,
		1.00f, 1.00f, 1.00f,
	};
	GLushort cake_indices[] = {
		0, 1, 2, 2, 3, 0,	// Near face
		0, 4, 5, 5, 1, 0,	// Right face
		1, 5, 6, 6, 2, 1,	// Top face
		2, 6, 7, 7, 3, 2,	// Left face
		3, 7, 4, 4, 0, 3,	// Bottiom face
		4, 5, 6, 6, 7, 4,	// Far face
	};
	glGenVertexArrays(1, &cake_vao);
	glBindVertexArray(cake_vao);
	glGenBuffers(2, cake_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, cake_vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cake_vertices), cake_vertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	glBindBuffer(GL_ARRAY_BUFFER, cake_vbo[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cake_colors), cake_colors, GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	glGenBuffers(1, &cake_indices_vbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cake_indices_vbo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cake_indices) * sizeof(GLushort), cake_indices, GL_STATIC_DRAW);

	const GLfloat berry_vertices[] = {
		-0.9f, +0.5f, -2.4f,	// Far edge
		-0.6f, +0.5f, -2.4f,
		-0.3f, +0.5f, -2.4f,
		0.0f, +0.5f, -2.4f,
		+0.3f, +0.5f, -2.4f,
		+0.6f, +0.5f, -2.4f,
		+0.9f, +0.5f, -2.4f,	// Right edge
		+0.9f, +0.5f, -2.1f,
		+0.9f, +0.5f, -1.8f,
		+0.9f, +0.5f, -1.5f,
		+0.9f, +0.5f, -1.2f,
		+0.9f, +0.5f, -0.9f,
		+0.9f, +0.5f, -0.6f,
		+0.9f, +0.5f, -0.3f,
		+0.9f, +0.5f, 0.0f,
		+0.9f, +0.5f, +0.3f,
		+0.9f, +0.5f, +0.6f,
		+0.9f, +0.5f, +0.9f,
		+0.9f, +0.5f, +1.2f,
		+0.9f, +0.5f, +1.5f,
		+0.9f, +0.5f, +1.8f,
		+0.9f, +0.5f, +2.1f,
		+0.9f, +0.5f, +2.4f,	// Near edge
		+0.6f, +0.5f, +2.4f,
		+0.3f, +0.5f, +2.4f,
		0.0f, +0.5f, +2.4f,
		-0.3f, +0.5f, +2.4f,
		-0.6f, +0.5f, +2.4f,
		-0.9f, +0.5f, +2.4f,	// Left edge
		-0.9f, +0.5f, +2.1f,
		-0.9f, +0.5f, +1.8f,
		-0.9f, +0.5f, +1.5f,
		-0.9f, +0.5f, +1.2f,
		-0.9f, +0.5f, +0.9f,
		-0.9f, +0.5f, +0.6f,
		-0.9f, +0.5f, +0.3f,
		-0.9f, +0.5f, 0.0f,
		-0.9f, +0.5f, -0.3f,
		-0.9f, +0.5f, -0.6f,
		-0.9f, +0.5f, -0.9f,
		-0.9f, +0.5f, -1.2f,
		-0.9f, +0.5f, -1.5f,
		-0.9f, +0.5f, -1.8f,
		-0.9f, +0.5f, -2.1f,
	};
	const GLfloat berry_colors[] = {
		0.55f, 0.09f, 0.09f,	// Far edge
		0.14f, 0.14f, 0.55f,
		0.55f, 0.09f, 0.09f,
		0.14f, 0.14f, 0.55f,
		0.55f, 0.09f, 0.09f,
		0.14f, 0.14f, 0.55f,
		0.55f, 0.09f, 0.09f,	// Right edge
		0.14f, 0.14f, 0.55f,
		0.55f, 0.09f, 0.09f,
		0.14f, 0.14f, 0.55f,
		0.55f, 0.09f, 0.09f,
		0.14f, 0.14f, 0.55f,
		0.55f, 0.09f, 0.09f,
		0.14f, 0.14f, 0.55f,
		0.55f, 0.09f, 0.09f,
		0.14f, 0.14f, 0.55f,
		0.55f, 0.09f, 0.09f,
		0.14f, 0.14f, 0.55f,
		0.55f, 0.09f, 0.09f,
		0.14f, 0.14f, 0.55f,
		0.55f, 0.09f, 0.09f,
		0.14f, 0.14f, 0.55f,
		0.55f, 0.09f, 0.09f,	// Near edge
		0.14f, 0.14f, 0.55f,
		0.55f, 0.09f, 0.09f,
		0.14f, 0.14f, 0.55f,
		0.55f, 0.09f, 0.09f,
		0.14f, 0.14f, 0.55f,
		0.55f, 0.09f, 0.09f,	// Left edge
		0.14f, 0.14f, 0.55f,
		0.55f, 0.09f, 0.09f,
		0.14f, 0.14f, 0.55f,
		0.55f, 0.09f, 0.09f,
		0.14f, 0.14f, 0.55f,
		0.55f, 0.09f, 0.09f,
		0.14f, 0.14f, 0.55f,
		0.55f, 0.09f, 0.09f,
		0.14f, 0.14f, 0.55f,
		0.55f, 0.09f, 0.09f,
		0.14f, 0.14f, 0.55f,
		0.55f, 0.09f, 0.09f,
		0.14f, 0.14f, 0.55f,
		0.55f, 0.09f, 0.09f,
		0.14f, 0.14f, 0.55f,
	};
	glGenVertexArrays(1, &berry_vao);
	glBindVertexArray(berry_vao);
	glGenBuffers(2, berry_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, berry_vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(berry_vertices), berry_vertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	glBindBuffer(GL_ARRAY_BUFFER, berry_vbo[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(berry_colors), berry_colors, GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	const GLfloat topping_vertices[] = {
		-0.8f, +0.5f, +1.1f,	// Top-left
		+0.8f, +0.5f, +1.1f,	// Top-right
		-0.8f, +0.5f, +2.3f,	// Bottom-left
		+0.8f, +0.5f, +2.3f,	// Bottom-right
	};
	const GLfloat topping_colors[] = {
		0.63f, 0.63f, 0.38f,	// Top
		0.63f, 0.63f, 0.38f,
		0.63f, 0.63f, 0.38f,	// Bottom
		0.63f, 0.63f, 0.38f,
	};
	GLushort topping_indices[] = {
		0, 1, 2,	// Top-left
		3, 1, 2,	// Bottom-right
	};
	glGenVertexArrays(1, &topping_vao);
	glBindVertexArray(topping_vao);
	glGenBuffers(2, topping_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, topping_vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(topping_vertices), topping_vertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	glBindBuffer(GL_ARRAY_BUFFER, topping_vbo[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(topping_colors), topping_colors, GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	glGenBuffers(1, &topping_indices_vbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, topping_indices_vbo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(topping_indices) * sizeof(GLushort), topping_indices, GL_STATIC_DRAW);

	const GLfloat tag_vertices[] = {
		-0.7f, +0.5f, +1.2f,	// B
		-0.5f, +0.5f, +1.2f,
		-0.7f, +0.5f, +1.7f,
		-0.5f, +0.5f, +1.7f,
		-0.7f, +0.5f, +2.2f,
		-0.4f, +0.5f, +1.7f,	// -
		-0.2f, +0.5f, +1.7f,
		-0.1f, +0.5f, +1.2f,	// D
		+0.1f, +0.5f, +1.2f,
		-0.1f, +0.5f, +2.2f,
		+0.4f, +0.5f, +1.2f,	// A
		+0.3f, +0.5f, +1.7f,
		+0.4f, +0.5f, +1.7f,
		+0.2f, +0.5f, +2.2f,
		+0.4f, +0.5f, +2.2f,
		+0.5f, +0.5f, +1.2f,	// Y
		+0.7f, +0.5f, +1.2f,
		+0.6f, +0.5f, +1.7f,
		+0.5f, +0.5f, +2.2f,
	};
	const GLfloat tag_colors[] = {
		1.00f, 1.00f, 1.00f,	// B
		1.00f, 1.00f, 1.00f,
		1.00f, 1.00f, 1.00f,
		1.00f, 1.00f, 1.00f,
		1.00f, 1.00f, 1.00f,
		1.00f, 1.00f, 1.00f,
		1.00f, 1.00f, 1.00f,	// -
		1.00f, 1.00f, 1.00f,
		1.00f, 1.00f, 1.00f,	// D
		1.00f, 1.00f, 1.00f,
		1.00f, 1.00f, 1.00f,
		1.00f, 1.00f, 1.00f,	// A
		1.00f, 1.00f, 1.00f,
		1.00f, 1.00f, 1.00f,
		1.00f, 1.00f, 1.00f,
		1.00f, 1.00f, 1.00f,	// Y
		1.00f, 1.00f, 1.00f,
		1.00f, 1.00f, 1.00f,
		1.00f, 1.00f, 1.00f,
	};
	GLushort tag_indices[] = {
		0, 1, 1, 2, 2, 3, 3, 4, 4, 0,	// B
		5, 6,							// -
		7, 8, 8, 9, 9, 7,				// D
		10, 13, 11, 12, 10, 14,			// A
		15, 17, 16, 18					// Y
	};
	glGenVertexArrays(1, &tag_vao);
	glBindVertexArray(tag_vao);
	glGenBuffers(2, tag_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, tag_vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(tag_vertices), tag_vertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	glBindBuffer(GL_ARRAY_BUFFER, tag_vbo[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(tag_colors), tag_colors, GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	glGenBuffers(1, &tag_indices_vbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, tag_indices_vbo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(tag_indices) * sizeof(GLushort), tag_indices, GL_STATIC_DRAW);
}

void paintGL(void)
{
	glClearColor(0.2f, 0.6f, 0.8f, 1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Projection matrix
	glm::mat4 projectionMatrix = glm::perspective(
		glm::radians(45.0f),
		4.0f / 3.0f,
		0.1f,
		100.0f
	);
	// View matrix
	glm::mat4 viewMatrix = glm::lookAt(
		glm::vec3(5, 5, 5),
		glm::vec3(0, 0, 0),
		glm::vec3(0, 1, 0)
	);
	// Model matrix
	x_rotate = x_correct + sin(glm::radians(horizontal));
	y_rotate = y_correct + sin(glm::radians(vertical));
	glm::mat4 translateMatrix = glm::translate(mat4(), vec3(x_translate * delta, y_translate * delta, -x_translate * delta));
	glm::mat4 rotateMatrix = glm::rotate(mat4(), y_rotate, vec3(0, 1, 0)) * glm::rotate(mat4(), x_rotate, vec3(1, 0, 0));
	glm::mat4 scaleMatrix = glm::scale(mat4(), vec3(1.0f + scale * delta, 1.0f + scale * delta, 1.0F + scale * delta));
	glm::mat4 modelMatrix = translateMatrix * rotateMatrix * scaleMatrix;

	// projection
	glm::mat4 projection = projectionMatrix * viewMatrix * modelMatrix;
	GLuint projectionUniformLocation = glGetUniformLocation(programID, "projection");
	glUniformMatrix4fv(projectionUniformLocation, 1, GL_FALSE, &projection[0][0]);

	glBindVertexArray(cake_vao);
	glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(berry_vao);
	glEnable(GL_POINT_SMOOTH);
	glPointSize(15.0f * (1 + scale * delta));
	glDrawArrays(GL_POINTS, 0, 44);

	// B-day tag with inclination
	glm::mat4 postTranslate = glm::translate(mat4(), vec3(0, 0.9, -0.8));
	glm::mat4 postRotate = glm::rotate(mat4(), glm::radians(30.0f), vec3(1, 0, 0));
	projection *= postRotate * postTranslate;
	glUniformMatrix4fv(projectionUniformLocation, 1, GL_FALSE, &projection[0][0]);

	glBindVertexArray(topping_vao);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(tag_vao);
	glLineWidth(5.0f);
	glDrawElements(GL_LINES, 28, GL_UNSIGNED_SHORT, 0);

	glFlush();
	glutPostRedisplay();
}

void initializedGL(void) //run only once
{
	sendDataToOpenGL();
	installShaders();
}

int main(int argc, char *argv[])
{
	/*Initialization*/
	glutInit(&argc, argv);
	glutInitWindowPosition(100, 100);
	glutInitWindowSize(640, 480);
	glutCreateWindow("Assignment 1");
	glewInit();

	/*Register different CALLBACK function for GLUT to response
	with different events, e.g. window sizing, mouse click or
	keyboard stroke */
	initializedGL();
	glutDisplayFunc(paintGL);
	glutKeyboardFunc(keyboard);
	glutSpecialFunc(specialKey);
	glutMouseFunc(mouse);

	/*Enter the GLUT event processing loop which never returns.
	it will call different registered CALLBACK according
	to different events. */
	glutMainLoop();

	return 0;
}