	CSCI3260 Assignment 1 Keyboard / Mouse Events

Name: CHAN Ka Yi
Student ID: 1155093641

Manipulation: All the objects will be controlled and transformed at the same time.
	// Scaling
	Scroll up   :	zoom in
	Scroll down :	zoom out

	// Rotation
	Key "left"  :	rotate clockwise along y-axis
	Key "up"    :	rotate clockwise along x-axis
	Key "down"  :	rotate anti-clockwise along x-axis
	Key "right" :	rotate anti-clockwise along y-axis

	// Translation
	Key "a"     :	translate leftwards
	Key "w"     :	translate upwards
	Key "s"     :	translate downwards
	Key "d"     :	translate rightwards