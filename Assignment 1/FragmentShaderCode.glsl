#version 430 // GLSL version your computer supports

in vec3 fragmentColor;

out vec3 color;

void main()
{
	color = fragmentColor;	
}
