#version 430  // GLSL version your computer supports

in layout(location = 0) vec3 position;
in layout(location = 1) vec3 vertexColor;

uniform mat4 projection;

out vec3 fragmentColor;

void main()
{
	vec4 newPosition = vec4(position, 1);
	gl_Position =  projection * newPosition;
	fragmentColor = vertexColor;
}