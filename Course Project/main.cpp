#define _CRT_SECURE_NO_WARNINGS

#define ROCK 300
#define RING 4

#include "Dependencies\glew\glew.h"
#include "Dependencies\freeglut\freeglut.h"
#include "Dependencies\glm\glm.hpp"
#include "Dependencies\glm\gtc\matrix_transform.hpp"
#include "Dependencies\glm\gtc\type_ptr.hpp"

#include <iostream>
#include <fstream>
#include <vector>

#include "model.h"
#include "shader.h"
#include "interaction.h"

using namespace std;
using glm::vec3;
using glm::vec4;
using glm::mat4;

// program ID
GLint generalID, skyboxID;
// skybox
GLuint skybox_vao, skybox_vbo, skybox_texture;
// object
pipeline earth, wonderStar, rock[ROCK], ring[RING], spacecraft;
// self-rotation
float timer, planetRotate, rockRotate, ringRotate;
// visual feedback
GLuint visualFeedback, originalTexture;
float threshold = 0.8;

void drawVAO(pipeline buffer) {
	setInt(generalID, "mapping", 0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, buffer.texture);
	if (buffer.normalMapping) {
		setBool(generalID, "normalMapping", true);
		setInt(generalID, "mapping_N", 1);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, buffer.normalTexture);
	}
	else setBool(generalID, "normalMapping", false);
	glBindVertexArray(buffer.vao);
	glDrawArrays(GL_TRIANGLES, 0, buffer.size);
}

bool collisionTest(glm::mat4 matrixA, glm::mat4 matrixB)
{
	vec4 vectorA = matrixA * vec4(0, 0, 0, 1);
	vec4 vectorB = matrixB * vec4(0, 0, 0, 1);
	if (length(vectorA - vectorB) <= threshold)
		return true;
	else return false;
}

void checkCollision(pipeline *buffer, glm::mat4 model, glm::mat4 spacecraft)
{
	if (buffer->collision) return;
	if (collisionTest(model, spacecraft)) {
		buffer->collision = true;
		return;
	}
	setMat4(generalID, "model", model); 
	drawVAO(*buffer);
}

void sendDataToOpenGL() 
{
	bool res;
	object obj;
	GLuint texture;

	// create skybox obj
	GLfloat skybox_vertices[] = {
		// Front face
		+50.0f, +50.0f, -50.0f,
		-50.0f, +50.0f, -50.0f,
		+50.0f, -50.0f, -50.0f,
		-50.0f, -50.0f, -50.0f,
		+50.0f, -50.0f, -50.0f,
		-50.0f, +50.0f, -50.0f,
		// Back face
		+50.0f, +50.0f, +50.0f,
		-50.0f, +50.0f, +50.0f,
		+50.0f, -50.0f, +50.0f,
		-50.0f, -50.0f, +50.0f,
		+50.0f, -50.0f, +50.0f,
		-50.0f, +50.0f, +50.0f,
		// Bottom face
		+50.0f, -50.0f, +50.0f,
		-50.0f, -50.0f, +50.0f,
		+50.0f, -50.0f, -50.0f,
		-50.0f, -50.0f, -50.0f,
		+50.0f, -50.0f, -50.0f,
		-50.0f, -50.0f, +50.0f,
		// Top face
		+50.0f, +50.0f, +50.0f,
		-50.0f, +50.0f, +50.0f,
		+50.0f, +50.0f, -50.0f,
		-50.0f, +50.0f, -50.0f,
		+50.0f, +50.0f, -50.0f,
		-50.0f, +50.0f, +50.0f,
		// Left face
		-50.0f, +50.0f, +50.0f,
		-50.0f, +50.0f, -50.0f,
		-50.0f, -50.0f, +50.0f,
		-50.0f, -50.0f, -50.0f,
		-50.0f, -50.0f, +50.0f,
		-50.0f, +50.0f, -50.0f,
		// Right face
		+50.0f, +50.0f, +50.0f,
		+50.0f, +50.0f, -50.0f,
		+50.0f, -50.0f, +50.0f,
		+50.0f, -50.0f, -50.0f,
		+50.0f, -50.0f, +50.0f,
		+50.0f, +50.0f, -50.0f,		
	};
	vector<const GLchar*> skybox_faces;
	skybox_faces.push_back("skybox/right.bmp");
	skybox_faces.push_back("skybox/left.bmp");
	skybox_faces.push_back("skybox/top.bmp");
	skybox_faces.push_back("skybox/bottom.bmp");
	skybox_faces.push_back("skybox/back.bmp");
	skybox_faces.push_back("skybox/front.bmp");
	skybox_texture = loadCubeMap(skybox_faces);
	// create skybox vao & vbo
	glGenVertexArrays(1, &skybox_vao);
	glBindVertexArray(skybox_vao);
	glGenBuffers(1, &skybox_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, skybox_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(skybox_vertices), &skybox_vertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	// create earth obj
	res = loadOBJ("object/planet.obj", &obj);
	texture = loadTexture("texture/earthTexture.bmp");
	generateVAOVBO(obj, &earth, texture);
	earth.normalTexture = loadTexture("texture/earthNormal.bmp");
	earth.normalMapping = true;
	clear(&obj);

	// create wonderStar obj
	res = loadOBJ("object/planet.obj", &obj);
	texture = loadTexture("texture/wonderStarTexture.bmp");
	generateVAOVBO(obj, &wonderStar, texture);
	clear(&obj);

	// create rock obj
	res = loadOBJ("object/rock.obj", &obj);
	texture = loadTexture("texture/rockTexture.bmp");
	for (int i = 0; i < ROCK; i++)
		generateVAOVBO(obj, &rock[i], texture);
	clear(&obj);

	// create ring obj
	res = loadOBJ("object/ring.obj", &obj);
	texture = loadTexture("texture/ringTexture.bmp");
	for (int i = 0; i < RING; i++)
		generateVAOVBO(obj, &ring[i], texture);
	clear(&obj);

	// create spacecraft obj
	res = loadOBJ("object/spacecraft.obj", &obj);
	originalTexture = loadTexture("texture/spacecraftTexture.bmp");
	generateVAOVBO(obj, &spacecraft, originalTexture);
	clear(&obj);

	// visual feedback
	visualFeedback = loadTexture("texture/visualFeedback.bmp");
}

void paintGL(void) 
{
	bool flag = false;
	glClearColor(0.0f, 0.0f, 0.8f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// spacecraft modelling
	glm::mat4 translateMatrix = glm::translate(mat4(), vec3(x_translate, 0.5, 20 + z_translate));
	glm::mat4 rotateMatrix = glm::rotate(mat4(), glm::radians(y_rotate), vec3(0, 1, 0));
	glm::mat4 scaleMatrix = glm::scale(mat4(), vec3(0.0005f, 0.0005f, 0.0005f));
	glm::mat4 spacecraftModel = translateMatrix * rotateMatrix;
	// world space modelling
	glm::vec4 camera = spacecraftModel * vec4(0.0f, 0.5f, 0.8f, 1.0f);
	glm::vec4 viewport = spacecraftModel * vec4(0.0f, 0.0f, -0.8f, 1.0f);
	// Projection matrix
	glm::mat4 projection = glm::perspective(glm::radians(45.0f), 4.0f / 3.0f, 0.1f, 500.0f);
	glm::mat4 view = glm::lookAt(glm::vec3(camera), glm::vec3(viewport), glm::vec3(0, 1, 0));
	// Model matrix
	glm::mat4 model;
	spacecraftModel = spacecraftModel * glm::rotate(mat4(), glm::radians(180.0f), vec3(0, 1, 0)) * scaleMatrix;

	// Lighting
	setFloat(generalID, "ambientControl", ambient);
	setFloat(generalID, "diffuseControl", diffuse);
	setFloat(generalID, "specularControl", specular);
	setVec3(generalID, "viewPort", glm::vec3(x_translate, 1.25, 23 + z_translate));
	setVec3(generalID, "lightSource[0]", glm::vec3(-10.0f, 15.0f, 25.0f));
	setVec3(generalID, "lightColor[0]", glm::vec3(1.0f, 1.0f, 1.0f));
	setVec3(generalID, "lightSource[1]", glm::vec3(0.0f, 15.0f, 0.0f));
	setVec3(generalID, "lightColor[1]", glm::vec3(0.5f, 0.5f, 0.5f));
	setVec3(generalID, "lightSource[2]", glm::vec3(10.0f, 15.0f, -25.0f));
	setVec3(generalID, "lightColor[2]", glm::vec3(1.0f, 0.5f, 0.0f));

	glDepthMask(GL_FALSE);
	glUseProgram(skyboxID);
	setMat4(skyboxID, "projection", projection);
	setMat4(skyboxID, "view", view);
	// skybox
	setInt(skyboxID, "mapping", 0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, skybox_texture);
	glBindVertexArray(skybox_vao);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
	glDepthMask(GL_TRUE);
	
	glEnable(GL_DEPTH_TEST);
	glUseProgram(generalID);
	setMat4(generalID, "projection", projection);
	setMat4(generalID, "view", view);

	// auto-rotation
	timer = (float)glutGet(GLUT_ELAPSED_TIME) / 25;
	planetRotate = timer;
	rockRotate = timer / 500;
	ringRotate = timer * 1.5;

	// earth
	translateMatrix = glm::translate(mat4(), vec3(0, 0, -20));
	rotateMatrix = glm::rotate(mat4(), glm::radians(planetRotate), vec3(0, 1, 0));
	scaleMatrix = glm::scale(mat4(), vec3(0.5f, 0.5f, 0.5f));
	model = translateMatrix * rotateMatrix * scaleMatrix;
	checkCollision(&earth, model, spacecraftModel);

	// wonderStar
	translateMatrix = glm::translate(mat4(), vec3(0, 0.05, 15));
	rotateMatrix = glm::rotate(mat4(), glm::radians(planetRotate), vec3(0, 1, 0));
	scaleMatrix = glm::scale(mat4(), vec3(0.35f, 0.35f, 0.35f));
	model = translateMatrix * rotateMatrix * scaleMatrix;
	checkCollision(&wonderStar, model, spacecraftModel);

	// rock
	srand(12345678);
	GLfloat radius = 3;
	for (int i = 0; i < ROCK; i++) {
		GLfloat offset = rand() % 10 / 30.0f;
		GLfloat theta = (float)i / (float)ROCK * 360 + rockRotate;
		translateMatrix = glm::translate(mat4(), vec3(sin(theta) * radius + offset, 0.55 - offset, 15 + cos(theta) * radius * 0.5 + offset));
		GLfloat phi = rand() % 360 + planetRotate;
		rotateMatrix = glm::rotate(mat4(), glm::radians(phi), vec3(offset, offset, offset));
		GLfloat scale = rand() % 10 / 200.0f;
		scaleMatrix = glm::scale(mat4(), vec3(scale, scale, scale));
		model = translateMatrix * rotateMatrix * scaleMatrix;
		checkCollision(&rock[i], model, spacecraftModel);
	}

	// ring
	for (int i = RING - 1; i >= 0; i--) {
		translateMatrix = glm::translate(mat4(), vec3(0, 0.5, 7.5 - 7 * i));
		rotateMatrix = glm::rotate(mat4(), glm::radians(90.0f), vec3(1, 0, 0)) * glm::rotate(mat4(), glm::radians(ringRotate), vec3(0, 0, 1));
		scaleMatrix = glm::scale(mat4(), vec3(0.015 - 0.001 * i, 0.015 - 0.001 * i, 0.015 - 0.001 * i));
		model = translateMatrix * rotateMatrix * scaleMatrix;
		setMat4(generalID, "model", model);
		if (collisionTest(model, spacecraftModel)) {
			flag = true;
			ring[i].texture = visualFeedback;
		}
		drawVAO(ring[i]);
	}

	// spacecraft
	setMat4(generalID, "model", spacecraftModel);
	if (flag) spacecraft.texture = visualFeedback;
	else spacecraft.texture = originalTexture;
	drawVAO(spacecraft);

	glFlush();
	glutPostRedisplay();
}

void initializedGL(void)
{
	glewInit();
	generalID = installShaders("VertexShaderCode.glsl", "FragmentShaderCode.glsl");
	skyboxID = installShaders("SkyboxVertexShaderCode.glsl", "SkyboxFragmentShaderCode.glsl");
	sendDataToOpenGL();
}

int main(int argc, char *argv[])
{
	glutInit(&argc, argv);
	glutInitWindowPosition(250, 50);
	glutInitWindowSize(720, 540);
	glutCreateWindow("Course Project");

	initializedGL();
	glutDisplayFunc(paintGL);

	glutKeyboardFunc(lighting);
	glutSpecialFunc(movement);
	glutPassiveMotionFunc(turning);

	glutMainLoop();

	return 0;
}