	CSCI3260 Course Project Rotating Energy Rings

Name: CHAN Ka Yi
Student ID: 1155093641

Manipulation:
	// Lighting control
	Key "q"      :	increase the brightness of ambient light
	Key "w"      :	decrease the brightness of ambient light
	Key "a"      :	increase the brightness of diffuse light
	Key "s"      :	decrease the brightness of diffuse light
	Key "z"      :	increase the brightness of specular light
	Key "x"      :	decrease the brightness of specular light

	// Model control
	Key "up"     :	translate the spacecraft forewards
	Key "down"   :	translate the spacecraft backwards
	Key "left"   :	translate the spacecraft leftwards
	Key "right"  :	translate the spacecraft rightwards
	Scroll left  :	rotate the spacecraft leftwards
	Scroll right :	rotate the spacecraft rightwards