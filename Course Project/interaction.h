#ifndef INTERACTION_H
#define INTERACTION_H

#include "Dependencies\glew\glew.h"
#include "Dependencies\freeglut\freeglut.h"
#include "Dependencies\glm\glm.hpp"
#include "Dependencies\glm\gtc\matrix_transform.hpp"
#include "Dependencies\glm\gtc\type_ptr.hpp"

#include <iostream>
#include <fstream>
#include <vector>

using namespace std;
using glm::vec3;
using glm::vec4;
using glm::mat4;

// transformation
float x_translate = 0;
float z_translate = 0;
float y_rotate = 0;

// light control
float ambient = 0.15;
float diffuse = 0.65;
float specular = 0.35;

void lighting(unsigned char key, int x, int y)
{
	if (key == 'q') {
		ambient += 0.05;
		if (ambient > 1)
			ambient -= 0.05;
	}
	if (key == 'w') {
		ambient -= 0.05;
		if (ambient < 0)
			ambient += 0.05;
	}
	if (key == 'a') {
		diffuse += 0.05;
		if (diffuse > 1)
			diffuse -= 0.05;
	}
	if (key == 's') {
		diffuse -= 0.05;
		if (diffuse < 0)
			diffuse += 0.05;
	}
	if (key == 'z') {
		specular += 0.05;
		if (specular > 1)
			specular -= 0.05;
	}
	if (key == 'x') {
		specular -= 0.05;
		if (specular < 0)
			specular += 0.05;
	}
}

void movement(int key, int x, int y)
{
	// direction matrix
	glm::mat4 directionMatrix = glm::rotate(mat4(), glm::radians(y_rotate), vec3(0, 1, 0));
	glm::vec4 front = directionMatrix * vec4(0.0f, 0.0f, -1.0f, 1.0f);
	front = glm::normalize(front);
	glm::vec4 right = directionMatrix * vec4(1.0f, 0.0f, 0.0f, 1.0f);
	right = glm::normalize(right);

	if (key == GLUT_KEY_UP) {
		x_translate += 0.25 * front[0];
		z_translate += 0.25 * front[2];
	}
	if (key == GLUT_KEY_DOWN) {
		x_translate -= 0.25 * front[0];
		z_translate -= 0.25 * front[2];
	}
	if (key == GLUT_KEY_LEFT) {
		x_translate -= 0.25 * right[0];
		z_translate -= 0.25 * right[2];
	}
	if (key == GLUT_KEY_RIGHT) {
		x_translate += 0.25 * right[0];
		z_translate += 0.25 * right[2];
	}
}

void turning(int x, int y)
{
	y_rotate = (float)(360 - x) / 6;
}

#endif // !INTERACTION_H
