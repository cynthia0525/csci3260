#version 430 // GLSL version your computer supports

in vec3 vertexFrag;

uniform samplerCube mapping;

out vec4 colorFrag;

void main()
{
	colorFrag = texture(mapping, vertexFrag);
}
